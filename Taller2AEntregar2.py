
#Funciones Auxiliares

#a) devuelve True si n1 es igual a n2
def iguales(n1, n2):
    if n1 == n2:
        return True
    return False

    
cc = "Lista de Caracteres"
cadenaVacia = ""
cc2 = "Oracion mas larga para ver que pasa con una oracion mas larga" 
cc3 = "Chequeando el papapa papapa"


# b)
def Coincide(frase, palabra, i):
    j = 0
    
    while j < len(palabra) and iguales(frase[i + j], palabra[j]):
        j = j + 1
        
    if iguales(j, len(palabra)):
        return True
    return False


# print(Coincide(cc, "de", 3))
# print(Coincide(cc, "de", 6))

 
lista1 = [1.2, 1.4, 1.6, 1.8] 
lista2 = [1.1, 1.5, 1.6, 2.0]

lista3 = [7, 5, 3, 6, 4, 2]
lista4 = [3, 3]

# c)
def posicionMinimo(a):
    i = 1
    res = a[0]
    posicion = 0
    while i < len(a):
        if a[i] < res:
            res = a[i]
            posicion = i
        i = i + 1   
    return posicion

# print(posicionMinimo(lista1))
# print(posicionMinimo(lista3))



# 1) cantSub una funcion llamada cantSub que recibe dos cadenas de caracteres
# y devuelve la cantidad de veces que aparece la segunda en la primera

##Especificacion##

#Predicado Auxiliar: 
#   Coincide(frase, palabra, i) = (Vj:Z)(0 <= j < |palabra| ---> frase[i + j] == palabra[j]))
#(no limito i a la long de la frase, porque eso va a ocurrir en el predicado principal. Pero asi suelta, el predicado
# auxiliar puede indefinirse si se brinda un i fuera del rango de la longitud de la frase)

# Problema cantSub(c1, c2 :Ch) == Res:Z {
#     Req |c2|> 0
#     Asegura (Sum[i = 0, |c1| - |c2|] B(Coincide(f, p, i)))  == Res
#     }


##Implementación##

def cantSub(c1, c2):
    i = 0
    contador = 0
    while i < len(c1) - (len(c2) - 1):
        if Coincide(c1, c2, i):
            contador = contador + 1
        i = i + 1
    return contador    

# print(cc)    
# print(cc2)
# print(cc3)

# print(cantSub(cc, "Lista"))    
# print(cantSub(cc2, "larga"))
# print(cantSub(cc2, "ar")) 
# print(cantSub(cc2, " ")) 
# print(cantSub(cadenaVacia, "Lista"))
# print(cantSub(cc3, "papa"))
# print(cantSub(cc, ["L", "i", "s", "t", "a"]))


Frase = "¡Vienen los zombies!, no quiero que un zombie se coma mi brazo, porque sino me volvere un zombie que come otro brazo"
Palabra = "zombie"
print(Frase)
print (Palabra)
print(cantSub(Frase, Palabra))


##########################


#2) que recibe dos listas de numeros ordenadas y devuelve otra lista que corresponde con la union ordenada de las recibidas

##Especificacion##

# Pred Aux: 
#   esCreciente ([l:R]) == ((Vi:Z)(0 <= i < |Res - 1| ---> Res[i] <= Res[i + 1]))

# Pred Aux: 
#   cantidadApariciones([a:R], x:R) == (Sumat[i=0; |a|-1] Beta(a[i] == x))


# Problema unirProlijo ([l1:R], [l2:R]) == [Res:R]{
#   Requiere esCreciente(l1) and esCreciente(l2)
#   Asegura len |l1| + len|l2| == len|Res| and
#   esCreciente(Res) and
#   (Vi:Z)(((0 <= i < |l1| -----> cantidadApariciones(l1, l1[i])) + (cantidadApariciones(l2, l1[i]))) == 
#   cantidadApariciones(Res, l1[i])) and (((0 <= i < |l2| -----> cantidadApariciones(l2, l2[i])) + (cantidadApariciones(l1, l2[i]))) 
#   == cantidadApariciones(Res, l2[i])) 
#   }


##Implementación##

def unirProlijo(l1, l2):

    Res = []
    i = 0
    j = 0
    
    while i < len(l1) and j < len(l2):
        
        if l2[j] <= l1[i]:
            Res.append(l2[j])
            j = j + 1
        
        elif l2[j] > l1[i]:
            Res.append(l1[i])
            i = i + 1
    
    if i == len(l1):
        while j < len(l2):
            Res.append(l2[j])
            j = j + 1
            
    elif j == len(l2):
        while i < len(l1):
            Res.append(l1[i])
            i = i + 1        
            
    return Res      



# print(lista1)
# print(lista2)
# print(lista4)

# print(unirProlijo(lista1, lista4))
# print(unirProlijo(lista1, [])) 
# print(unirProlijo([], lista2))  


print(unirProlijo(lista1, lista2))