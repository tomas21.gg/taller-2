#Funciones Auxiliares

#a) devuelve True si n1 es igual a n2
def iguales(n1, n2):
    if n1 == n2:
        return True
    return False


#b) devuelve la cantidad de veces que aparece el elemento x en la lista a
def cantidadApariciones(a, x): 
    i = 0
    cuenta = 0
    while i < len(a):
        if iguales(a[i], x):
            cuenta = cuenta + 1
        i = i + 1
    return cuenta


lista1 = [1.2, 1.4, 1.6, 1.8] 
lista2 = [1.1, 1.5, 1.6, 2.0]
lista4 = [3, 3]

# d)
def posicionMinimo(a):
    i = 1
    res = a[0]
    posicion = 0
    while i < len(a):
        if a[i] < res:
            res = a[i]
            posicion = i
        i = i + 1   
    return posicion

# print(posicionMinimo(lista1))
# print(posicionMinimo(lista3))


# e)
#Recibe dos listas, y chequea si alguna o ambas se encuentra/n vacias. Si ambas estan vacias, retorna una lista vacia.
# Si sólo una está vacia, retorna la otra 
def AccionSiListaVacia(l1, l2):
    
    if len(l1) == 0 and len(l2) == 0:
        return []

    elif len(l1) == 0:
        return l2

    elif len(l2) == 0:
        return l1




# 1) cantSub una funcion llamada cantSub que recibe dos cadenas de caracteres
# y devuelve la cantidad de veces que aparece la segunda en la primera

##Especificacion##

#Predicado Auxiliar: 
#   Coincide(frase, palabra, i) = (Vj:Z)(0 <= j < |palabra| --- frase[j] == palabra[j+i]) == Res )

# Problema cantSub(c1, c2 :Ch) == Res:Z {
#     Req |c2|> 0
#     Asegura Sum[i=0, |a| - |b|] B(coincide(f, p, i))
#     }

#Con 'Ch' refiero a cadena de caracteres ('string'). Mi código funciona bien si entran dos variables 'string', 
#y no anda bien si una entra como string y la otra como lista de caracteres


##Implementación##
cc = "Lista de Caracteres"
cadenaVacia = ""
cc2 = "Oracion mas larga para ver que pasa con una oracion mas larga" 

def cantSub(c1, c2):
    i = 0
    j = 1
    contador = 0
    while i < len(c1) - 1:
        j = i + 1
        while j <= len(c1):
            if iguales(c1[i:j], c2):
                contador = contador + 1
            j = j + 1
        i = i + 1
    return contador

# print(cantSub(cc, "Lista"))    
# print(cantSub(cc2, "larga"))
# print(cantSub(cc2, "oracion")) 
# # print(cantSub(cc2, " ")) 
# print(cantSub(cadenaVacia, "Lista"))

# Frase = "¡Vienen los zombies!, no quiero que un zombie se coma mi brazo, porque sino me volvere un zombie que come otro brazo"
# Palabra = "zombie"
# print(cantSub(Frase, Palabra))

# print(cantSub(cc, ["L", "i", "s", "t", "a"]))

#2) que recibe dos listas de numeros ordenadas y devuelve otra lista que corresponde con la union ordenada de las recibidas

# Pred Aux: 
#   esCreciente ([l:R]) == ((Vi:Z)(0 <= i < |Res - 1| ---> Res[i] <= Res[i + 1]))

# Pred Aux: 
#   cantidadApariciones([a:R], x:R) == (Sumat[i=0; |a|-1] Beta(a[i] == x))


##Especificacion##
# Problema unirProlijo ([l1:R], [l2:R]) == [Res:R]{
#   Requiere esCreciente(l1) and esCreciente(l2)
#   Asegura len |l1| + len|l2| == len|Res| and
#   esCreciente(Res) and
#   (Vi:Z)(((0 <= i < |l1| -----> cantidadApariciones(l1, l1[i])) + (cantidadApariciones(l2, l1[i]))) == 
#   cantidadApariciones(Res, l1[i])) and (((0 <= i < |l2| -----> cantidadApariciones(l2, l2[i])) + (cantidadApariciones(l1, l2[i]))) 
#   == cantidadApariciones(Res, l2[i])) 
#   }



##Implementación##

#Version 1# Esta muy bien, pero revisa las dos listas varias veces. No está aprovechando el hecho de que las listas input llegan ordenadas
# def unirProlijo(l1, l2):
#     lista = []
#     ambasListas = l1 + l2
    
#     while len(ambasListas) > 0:
#         lista.append(ambasListas.pop(posicionMinimo(ambasListas)))
    
#     return lista


#Versión 2# Esta version compara sólo primeros valores de cada lista, ordenarla la suma de las dos listas. Mejor en términos de eficiencia
# La funcion auxiliar podria no ir, y la funcion computaria el mismo resultado. La utilidad de la funcion auxiliar es no correr
# todo el computo si desde el vamos una de la listas se encuentra vacia. Sin la funcion auxiliar, si por ejemplo el usuario 
#ingresara una lista de longitud 5600 y otra lista de longitud 0, la funcion terminaria agregando esos 5600 valores en el mismo orden
# a la lista res. En cambio con la funcion auxiliar, ya viendo que una de las listas esta vacia, directamente retorna la otra.
def unirProlijo(l1, l2):
    
    if len(l1) == 0 or len(l2) == 0:
        return AccionSiListaVacia(l1, l2)
    
    else:
        
        Res = []
        i = 0
        j = 0
        
        while i < len(l1) and j < len(l2):
            
            if l2[j] <= l1[i]:
                Res.append(l2[j])
                j = j + 1
            
            elif l2[j] > l1[i]:
                Res.append(l1[i])
                i = i + 1
        
        if i == len(l1):
            while j < len(l2):
                Res.append(l2[j])
                j = j + 1
                
        elif j == len(l2):
            while i < len(l1):
                Res.append(l1[i])
                i = i + 1        
                
        return Res      



# print(lista1)
# print(lista2)
# print(lista4)
# print(unirProlijo(lista1, lista2))
# print(unirProlijo(lista1, lista4))
# print(unirProlijo(lista1, [])) 
# print(unirProlijo([], lista2))  